import streamlit as st
import openai
from dotenv import load_dotenv
import os


if load_dotenv():
    print("Found OpenAI API Base Endpoint: " + os.getenv("AZURE_OPENAI_ENDPOINT"))
else: 
    print("OpenAI API Base Endpoint not found. Have you configured the .env file?")

# Get endpoint and key from environment variables
openai.api_base = os.environ['AZURE_OPENAI_ENDPOINT']
openai.api_key = os.environ['AZURE_OPENAI_API_KEY']     

# Assign the API version (DALL-E is currently supported for the 2023-06-01-preview API version only)
openai.api_version = '2023-06-01-preview'
openai.api_type = 'azure'    

st.title("My DallE")

prompt = st.text_input("Enter your prompt:")
generate_button = st.button("Generate Image")

if generate_button:
    st.write("Generating image...")
    st.write(prompt)

    generation_response = openai.Image.create(
          prompt=prompt,    # Enter your prompt text here
          size='1024x1024',
          n=2
    )
    image_url = generation_response["data"][0]["url"]
    st.image(image_url, caption="Generated Image", use_column_width=True)
