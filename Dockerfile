FROM python:3.11-slim
  
RUN apt-get update && apt-get install -y git

WORKDIR /app
 
COPY ./requirements.txt /app/requirements.txt
 
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./.env /app/.env
  
COPY ./azure_dalle.py /app/azure_dalle.py

EXPOSE 8501
 
CMD ["streamlit", "run", "/app/azure_dalle.py", "--server.port=8501", "--server.address=0.0.0.0"]