# Pre-requisite

1. Azure Subscription
2. Azure OpenAI Service Access
3. [Python Installation](https://www.python.org/downloads/)
4. [Visual Studio Code Installation](https://code.visualstudio.com/download)
5. create virtual python environment (Windows) (Mac os activate in ./venv/bin/activate)

   ```sh
   python -m venv venv
   source .\venv\Scripts\activate
   ```

6. Other Python Packages To Code The Application. Install the required packages to work with the code.
   `pip install -r requirements.txt`
7. In .env file and set values as below:

   ```sh
   AZURE_OPENAI_API_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxx
   AZURE_OPENAI_ENDPOINT=https://xxxxxx.openai.azure.com/
   ```

# How to run

```sh
streamlit run azure_dalle.py
```
